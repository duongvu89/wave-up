/*
 * Copyright (c) 2016-2019 Juan García Basilio
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup.prefs

import android.content.Context
import android.content.SharedPreferences
import android.preference.CheckBoxPreference
import android.preference.PreferenceActivity
import android.preference.PreferenceManager
import android.preference.SwitchPreference
import com.jarsilio.android.waveup.model.SingletonHolder
import com.jarsilio.android.waveup.prefs.PreferenceActivity.GeneralPreferenceFragment
import com.jarsilio.android.waveup.service.ProximitySensorHandler

class Settings private constructor(context: Context) {
    private val applicationContext: Context = context.applicationContext

    var preferenceActivity: PreferenceActivity? = null
    val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)

    var isServiceEnabled: Boolean
        get() = preferences.getBoolean(ENABLED, false)
        set(enabled) = setPreference(ENABLED, enabled)

    var isPaused: Boolean
        get() = preferences.getBoolean(PAUSED, false)
        set(paused) = preferences.edit().putBoolean(PAUSED, paused).apply()

    var isWaveMode: Boolean
        get() = preferences.getBoolean(WAVE_MODE, true)
        set(waveMode) = setPreference(WAVE_MODE, waveMode)

    var isPocketMode: Boolean
        get() = preferences.getBoolean(POCKET_MODE, true)
        set(pocketMode) = setPreference(POCKET_MODE, pocketMode)

    var isLockScreen: Boolean
        get() = preferences.getBoolean(LOCK_SCREEN, false)
        set(lockScreen) = setPreference(LOCK_SCREEN, lockScreen)

    var isLockScreenWhenLandscape: Boolean
        get() = preferences.getBoolean(LOCK_SCREEN_WHEN_LANDSCAPE, false)
        set(lockScreenWhenLandscape) = setPreference(LOCK_SCREEN_WHEN_LANDSCAPE, lockScreenWhenLandscape)

    var isLockScreenWithPowerButton: Boolean
        get() = preferences.getBoolean(LOCK_SCREEN_WITH_POWER_BUTTON, false)
        set(lockScreenWithPowerButton) = setPreference(LOCK_SCREEN_WITH_POWER_BUTTON, lockScreenWithPowerButton)

    var isVibrateWhileLocking: Boolean
        get() = preferences.getBoolean(VIBRATE_ON_LOCK, false)
        set(vibrateWhileLocking) = setPreference(VIBRATE_ON_LOCK, vibrateWhileLocking)

    var sensorCoverTimeBeforeLockingScreen: Long
        get() = preferences.getString(SENSOR_COVER_TIME_BEFORE_LOCKING_SCREEN, "1000")!!.toLong()
        set(sensorCoverTimeBeforeLocking) = setPreference(SENSOR_COVER_TIME_BEFORE_LOCKING_SCREEN, sensorCoverTimeBeforeLocking.toString())

    var isShowNotification: Boolean
        get() = preferences.getBoolean(SHOW_NOTIFICATION, true)
        set(showNotification) = setPreference(SHOW_NOTIFICATION, showNotification)

    var numberOfWavesToWaveUp: Long
        get() = preferences.getString(NUMBER_OF_WAVES, "2")!!.toLong()
        set(numberOfWaves) = setPreference(NUMBER_OF_WAVES, numberOfWaves.toString())

    private fun setPreference(key: String, value: Boolean) {
        // This changes the GUI, but it needs the MainActivity to have started
        val generalPreferenceFragment = preferenceActivity?.fragmentManager?.findFragmentById(android.R.id.content) as GeneralPreferenceFragment?
        val preference = generalPreferenceFragment?.findPreference(key)
        if (preference is CheckBoxPreference) {
            preference.isChecked = value
        } else if (preference is SwitchPreference) {
            preference.isChecked = value
        }

        // This doesn't change the GUI
        preferences.edit().putBoolean(key, value).apply()

        /* onSharedPreferenceChanged is not called sometimes when status of a preference is changed manually.
         * Call startOrStop here to check if proximity sensor listener should be registered or not. */
        ProximitySensorHandler.getInstance(applicationContext).startOrStopListeningDependingOnConditions()
    }

    private fun setPreference(key: String, value: String) {
        preferences.edit().putString(key, value).apply()
    }

    override fun toString(): String {
        return "{ " +
            "isServiceEnabled: $isServiceEnabled, " +
            "isPaused: $isPaused, " +
            "isWaveMode: $isWaveMode, " +
            "numberOfWavesToWaveUp: $numberOfWavesToWaveUp, " +
            "isPocketMode: $isPocketMode, " +
            "isLockScreen: $isLockScreen, " +
            "isLockScreenWhenLandscape: $isLockScreenWhenLandscape, " +
            "isLockScreenWithPowerButton: $isLockScreenWithPowerButton, " +
            "isVibrateWhileLocking: $isVibrateWhileLocking, " +
            "sensorCoverTimeBeforeLockingScreen: $sensorCoverTimeBeforeLockingScreen, " +
            "isShowNotification: $isShowNotification" +
            " }"
    }

    companion object : SingletonHolder<Settings, Context>(::Settings) {
        const val ENABLED = "pref_enable"
        const val PAUSED = "pref_paused"
        const val WAVE_MODE = "pref_wave_mode"
        const val POCKET_MODE = "pref_pocket_mode"
        const val LOCK_SCREEN = "pref_lock_screen"
        const val LOCK_SCREEN_WHEN_LANDSCAPE = "pref_lock_screen_when_landscape"
        const val LOCK_SCREEN_WITH_POWER_BUTTON = "pref_lock_screen_with_power_button_as_root"
        const val SENSOR_COVER_TIME_BEFORE_LOCKING_SCREEN = "pref_sensor_cover_time_before_locking_screen" // In milliseconds
        const val VIBRATE_ON_LOCK = "pref_lock_screen_vibrate_on_lock"
        const val NUMBER_OF_WAVES = "pref_number_of_waves"
        const val SHOW_NOTIFICATION = "pref_show_notification"
        const val SHOW_NOTIFICATION_V28 = "pref_show_notification_v28"
        const val EXCLUDED_APP_LIST = "pref_lock_screen_app_exception"
    }
}

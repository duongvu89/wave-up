/*
 * Copyright (c) 2016-2020 Juan García Basilio
 *
 * This file is part of WaveUp.
 *
 * WaveUp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * WaveUp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WaveUp.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.jarsilio.android.waveup

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.content.SharedPreferences
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import androidx.appcompat.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.jarsilio.android.common.dialog.Dialogs
import com.jarsilio.android.common.extensions.flavor
import com.jarsilio.android.common.extensions.isPieOrNewer
import com.jarsilio.android.common.menu.CommonMenu
import com.jarsilio.android.common.privacypolicy.PrivacyPolicyBuilder
import com.jarsilio.android.waveup.extensions.removeDeviceAdminPermission
import com.jarsilio.android.waveup.extensions.settings
import com.jarsilio.android.waveup.extensions.state
import com.jarsilio.android.waveup.prefs.PreferenceActivity
import com.jarsilio.android.waveup.prefs.Settings
import com.jarsilio.android.waveup.service.ProximitySensorHandler
import com.jarsilio.android.waveup.service.WaveUpService
import com.mikepenz.aboutlibraries.Libs
import com.mikepenz.aboutlibraries.LibsBuilder

import eu.chainfire.libsuperuser.Shell
import timber.log.Timber

const val REQUEST_LOCK_PERMISSIONS_ACTION = "com.jarsilio.android.waveup.action.REQUEST_LOCK_PERMISSIONS"

class MainActivity : PreferenceActivity(), SharedPreferences.OnSharedPreferenceChangeListener {
    private val proximitySensorHandler: ProximitySensorHandler by lazy { ProximitySensorHandler.getInstance(this) }
    private val permissionsHandler: PermissionsHandler by lazy { PermissionsHandler(this) }
    private val commonMenu: CommonMenu by lazy { CommonMenu(this) }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Timber.d("Starting WaveUp MainActivity (GUI)")

        if (intent.action == REQUEST_LOCK_PERMISSIONS_ACTION) {
            Timber.d("MainActivity started with a REQUEST_LOCK_PERMISSIONS_ACTION intent.action")
            permissionsHandler.openLockDevicePermissionExplanationIfNecessary()
        } else {
            if (!proximitySensorHandler.isProximitySensorAvailable()) {
                Timber.e("No proximity sensor found!")
                settings.isServiceEnabled = false // Just in case it was enabled at some point in history (which shouldn't be possible from now on)
                showNoProximitySensorAvailableAndFinish()
            } else {
                WaveUpService.start(this)
                registerPreferencesListener()
                Dialogs(this).showSomeLoveDialogIfNecessary()
            }

            Dialogs(this).showSoLongAndThanksForAllTheFishDialog()
        }
    }

    private fun showNoProximitySensorAvailableAndFinish() {
        AlertDialog.Builder(this).apply {
            setTitle(R.string.missing_proximity_sensor_title)
            setMessage(R.string.missing_proximity_sensor_text)
            setPositiveButton(android.R.string.yes) { _, _ -> finish() }
            setCancelable(false)
            show()
        }
    }

    private fun showAboutLicensesActivity() {
        var style = Libs.ActivityStyle.LIGHT_DARK_TOOLBAR
        var theme = R.style.AppTheme_About_Light

        val currentNightMode = resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        if (currentNightMode == Configuration.UI_MODE_NIGHT_YES) {
            style = Libs.ActivityStyle.DARK
            theme = R.style.AppTheme_About_Dark
        }

        LibsBuilder()
                .withActivityStyle(style)
                .withActivityTheme(theme)
                .withAboutIconShown(true)
                .withAboutVersionShown(true)
                .withActivityTitle(getString(R.string.licenses_menu_item))
                .withAboutDescription(getString(R.string.licenses_about_libraries_text))
                .start(this)
    }

    private fun showPrivacyPolicyActivity() {
        val privacyPolicyBuilder = PrivacyPolicyBuilder()
                .withIntro(getString(R.string.app_name), "Juan García Basilio (juanitobananas)")
                .withUrl("https://gitlab.com/juanitobananas/wave-up/blob/master/PRIVACY.md#waveup-privacy-policy")
                .withMeSection()
                .withEmailSection("juam+waveup@posteo.net")
                .withAutoGoogleOrFDroidSection()
                .withCustomSection(getString(R.string.privacy_policy_phone_permission))

        privacyPolicyBuilder.start(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        commonMenu.addImpressumToMenu(menu)
        commonMenu.addFaqToMenu(menu, "https://gitlab.com/juanitobananas/wave-up/blob/master/FAQ.md#faq")
        commonMenu.addSendDebugLogsToMenu(menu, "https://gitlab.com/juanitobananas/wave-up/blob/master/FAQ.md#faq")
        if (flavor == "fortuneCookies") {
            commonMenu.addCookiesToMenu(menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        when (item.itemId) {
            R.id.privacy_policy_menu_item -> showPrivacyPolicyActivity()
            R.id.licenses_menu_item -> showAboutLicensesActivity()
        }

        return super.onOptionsItemSelected(item)
    }

    @TargetApi(Build.VERSION_CODES.M)
    @SuppressLint("BatteryLife")
    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        proximitySensorHandler.startOrStopListeningDependingOnConditions()
        when (key) {
            Settings.ENABLED -> {
                if (settings.isServiceEnabled) {
                    settings.isPaused = false
                    permissionsHandler.openPhonePermissionExplanationIfNecessary()
                }
                WaveUpService.start(this)
                if (settings.isLockScreen) {
                    permissionsHandler.openLockDevicePermissionExplanationIfNecessary()
                }
            }

            Settings.LOCK_SCREEN -> {
                if (settings.isLockScreen) {
                    permissionsHandler.openLockDevicePermissionExplanationIfNecessary()
                }
            }

            Settings.LOCK_SCREEN_WITH_POWER_BUTTON -> if (settings.isLockScreenWithPowerButton) {
                settings.isLockScreenWithPowerButton = false
                Thread(Runnable {
                    if (!Shell.SU.available()) {
                        runOnUiThread {
                            Toast.makeText(this, R.string.root_access_failed, Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        runOnUiThread {
                            // This is a hack to avoid an infinite loop. Every time we set it to true, this case is executed.
                            unregisterPreferencesListener()
                            settings.isLockScreenWithPowerButton = true
                            registerPreferencesListener()
                        }
                    }
                }).start()
            }

            Settings.SHOW_NOTIFICATION -> {
                permissionsHandler.requestIgnoreBatteryOptimizationsIfNecessary()
                // Need to restart service to add or remove notification
                WaveUpService.restart(this)
            }
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            // This forces a re-draw. Useful when returning from requesting of permissions (device admin and accessibility mostly)
            settings.isLockScreen = settings.isLockScreen
        }
    }

    fun uninstallApp(view: View) {
        // The view parameter is just for the android:onClick attribute to be able to point here.
        if (state.isLockScreenAdmin) {
            removeDeviceAdminPermission()
            removeAdminRights = true
        }

        Timber.i("Uninstalling app")
        val packageURI = Uri.parse("package:$packageName")
        val uninstallIntent = Intent(Intent.ACTION_DELETE, packageURI)
        startActivityForResult(uninstallIntent, UNINSTALL_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            PermissionsHandler.DEVICE_ADMIN_REQUEST_CODE -> if (!state.isLockScreenAdmin) {
                // If the user does not activate lock admin switch off lock screen option
                settings.isLockScreen = false
            } else {
                proximitySensorHandler.startOrStopListeningDependingOnConditions()
            }
            UNINSTALL_REQUEST_CODE -> if (resultCode == Activity.RESULT_CANCELED && removeAdminRights) {
                val canceledMsg = Toast.makeText(this, R.string.removed_device_admin_rights, Toast.LENGTH_SHORT)
                canceledMsg.show()
                /* Show message UNINSTALL_CANCELED_MSG_SHOW_TIME second */
                object : CountDownTimer(UNINSTALL_CANCELED_MSG_SHOW_TIME.toLong(), UNINSTALL_CANCELED_MSG_SHOW_INTERVAL.toLong()) {
                    override fun onTick(millisUntilFinished: Long) {
                        canceledMsg.show()
                    }

                    override fun onFinish() {
                        canceledMsg.cancel()
                    }
                }.start()
                removeAdminRights = false
            }
            PermissionsHandler.BATTERY_OPTIMIZATION_REQUEST_CODE -> {
                if (!state.isIgnoringBatteryOptimizations) {
                    Timber.d("The user didn't accept the ignoring of the battery optimization. Forcing show_notification to true")
                    settings.isShowNotification = true
                }
                // Need to restart service to add or remove notification
                WaveUpService.restart(this)
            }
            PermissionsHandler.ACCESSIBILITY_SERVICE_REQUEST_CODE -> {
                if (!state.isAccessibilityServiceEnabled) {
                    Timber.d("The user didn't enable the AccessibilityService. Won't be able to lock device.")
                    settings.isLockScreen = false
                } else {
                    proximitySensorHandler.startOrStopListeningDependingOnConditions()
                }
            }
        }
    }

    private fun forceLockToFalseAndShowDialogIfNecessary() {
        if (isPieOrNewer && settings.isLockScreen && !state.isAccessibilityServiceEnabled ||
                !isPieOrNewer && settings.isLockScreen && !state.isLockScreenAdmin) {
            Timber.d("Forcing 'lock' option to false due to missing permissions.")
            settings.isLockScreen = false

            val alertDialog = AlertDialog.Builder(this).apply {
                setTitle(R.string.something_went_wrong)
                setPositiveButton(android.R.string.yes) { _, _ -> }
                setCancelable(false)
            }
            if (isPieOrNewer) {
                alertDialog.setMessage(R.string.lock_disabled_warning_accessibility_settings_text)
            } else {
                alertDialog.setMessage(R.string.lock_disabled_warning_device_admin_text)
            }
            alertDialog.show()
        }
    }

    override fun onResume() {
        super.onResume()
        settings.preferenceActivity = this
        registerPreferencesListener()
        forceLockToFalseAndShowDialogIfNecessary()
        settings.isServiceEnabled = settings.isServiceEnabled // Hack to reload settings. Sometimes won't show correct values if they were changed in the backgrpund (e.g. with Tasker)
    }

    override fun onPause() {
        super.onPause()
        settings.preferenceActivity = null
        unregisterPreferencesListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterPreferencesListener()
    }

    private fun registerPreferencesListener() {
        settings.preferences.registerOnSharedPreferenceChangeListener(this)
    }

    private fun unregisterPreferencesListener() {
        settings.preferences.unregisterOnSharedPreferenceChangeListener(this)
    }

    companion object {
        private const val UNINSTALL_REQUEST_CODE = 200
        private const val UNINSTALL_CANCELED_MSG_SHOW_TIME = 5000
        private const val UNINSTALL_CANCELED_MSG_SHOW_INTERVAL = 1000

        private var removeAdminRights = false
    }
}

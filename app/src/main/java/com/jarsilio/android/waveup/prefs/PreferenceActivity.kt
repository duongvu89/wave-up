package com.jarsilio.android.waveup.prefs

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.ListPreference
import android.preference.Preference
import android.preference.PreferenceCategory
import android.preference.PreferenceFragment
import android.preference.PreferenceManager
import com.jarsilio.android.common.extensions.isPieOrNewer
import com.jarsilio.android.waveup.PermissionsHandler
import com.jarsilio.android.waveup.R
import com.jarsilio.android.waveup.extensions.state
import com.jarsilio.android.waveup.model.AppCompatPreferenceActivity
import timber.log.Timber
import com.jarsilio.android.waveup.service.WaveUpService

/**
 * A [PreferenceActivity] that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 *
 * See [Android Design: Settings](http://developer.android.com/design/patterns/settings.html)
 * for design guidelines and the [Settings API Guide](http://developer.android.com/guide/topics/ui/settings.html)
 * for more information on developing a Settings UI.
 */
abstract class PreferenceActivity : AppCompatPreferenceActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentManager.beginTransaction().replace(android.R.id.content, GeneralPreferenceFragment()).commit()
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    override fun isValidFragment(fragmentName: String): Boolean {
        return PreferenceFragment::class.java.name == fragmentName ||
                GeneralPreferenceFragment::class.java.name == fragmentName
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    class GeneralPreferenceFragment : PreferenceFragment() {
        private val permissionsHandler: PermissionsHandler by lazy { PermissionsHandler(activity) }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            addPreferencesFromResource(R.xml.settings)
            setHasOptionsMenu(true)
            val notificationPreferenceCategory = findPreference("prefs_notification_category") as PreferenceCategory
            if (isPieOrNewer) {
                notificationPreferenceCategory.removePreference(findPreference(Settings.SHOW_NOTIFICATION))
            } else {
                notificationPreferenceCategory.removePreference(findPreference(Settings.SHOW_NOTIFICATION_V28))
            }
            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
            bindPreferenceSummaryToValue(findPreference(Settings.NUMBER_OF_WAVES))
            bindPreferenceSummaryToValue(findPreference(Settings.SENSOR_COVER_TIME_BEFORE_LOCKING_SCREEN))
            bindClickListeners()
        }

        private fun bindClickListeners() {
            // Excluded apps preference: open ExcludedAppsActivity (or ask for 'usage stats' permission if necessary)
            findPreference(Settings.EXCLUDED_APP_LIST).setOnPreferenceClickListener {
                permissionsHandler.openUsageAccessExplanationIfNecessary()
                true
            }

            // We can't hide the notification by not using a ForegroundService in P+ (just ignoring battery optimizations isn't enough)
            // We need to change the system settings. This opens the system's notification settings for WaveUp
            findPreference(Settings.SHOW_NOTIFICATION_V28)?.setOnPreferenceClickListener {
                Timber.d("Opening system notification settings for WaveUp.")

                val intent = Intent(android.provider.Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS).apply {
                    putExtra(android.provider.Settings.EXTRA_APP_PACKAGE, activity.packageName)
                    putExtra(android.provider.Settings.EXTRA_CHANNEL_ID, WaveUpService.NOTIFICATION_CHANNEL_ID)
                }

                startActivityForResult(intent, OPEN_NOFITICATION_SETTINGS_REQUEST_CODE)
                true
            }
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            when (requestCode) {
                OPEN_NOFITICATION_SETTINGS_REQUEST_CODE -> {
                    Timber.d("Returned from notification settings")
                    if (!activity.state.isIgnoringBatteryOptimizations) {
                        Timber.d("Requesting to ignore battery optimizations for WaveUp (this is always better, but mostly not crucial)")
                        startActivity(Intent(android.provider.Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS, Uri.parse("package:${activity.packageName}")))
                    }
                    // Need to restart service to add or remove notification
                    WaveUpService.restart(activity)
                }
            }
        }
    }

    companion object {
        private const val OPEN_NOFITICATION_SETTINGS_REQUEST_CODE = 500

        /**
         * A preference value change listener that updates the preference's summary
         * to reflect its new value.
         */
        private val sBindPreferenceSummaryToValueListener = Preference.OnPreferenceChangeListener { preference, value ->
            val stringValue = value.toString()

            if (preference is ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                val index = preference.findIndexOfValue(stringValue)

                // Set the summary to reflect the new value.
                preference.setSummary(
                        if (index >= 0)
                            preference.entries[index]
                        else
                            null)
            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.summary = stringValue
            }
            true
        }

        /**
         * Binds a preference's summary to its value. More specifically, when the
         * preference's value is changed, its summary (line of text below the
         * preference title) is updated to reflect the value. The summary is also
         * immediately updated upon calling this method. The exact display format is
         * dependent on the type of preference.

         * @see .sBindPreferenceSummaryToValueListener
         */
        private fun bindPreferenceSummaryToValue(preference: Preference) {
            // Set the listener to watch for value changes.
            preference.onPreferenceChangeListener =
                sBindPreferenceSummaryToValueListener

            // Trigger the listener immediately with the preference's
            // current value.
            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.context)
                            .getString(preference.key, ""))
        }
    }
}

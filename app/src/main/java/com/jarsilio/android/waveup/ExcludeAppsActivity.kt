package com.jarsilio.android.waveup

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import android.view.View
import androidx.cardview.widget.CardView
import com.jarsilio.android.waveup.adapter.AppListAdapter
import com.jarsilio.android.waveup.model.AppDatabase
import com.jarsilio.android.waveup.model.App
import com.jarsilio.android.waveup.model.AppsDao
import com.jarsilio.android.waveup.model.AppsViewModel
import com.jarsilio.android.waveup.model.AppsHandler
import com.jarsilio.android.waveup.model.EmptyRecyclerView

class ExcludeAppsActivity : AppCompatActivity() {
    private val appsHandler: AppsHandler by lazy { AppsHandler(this) }
    private val appsDao: AppsDao by lazy { AppDatabase.getInstance(this).appsDao() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_exclude_apps)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val excludedAppsRecyclerView = findViewById<EmptyRecyclerView>(R.id.recycler_excluded_apps)
        val emptyView = findViewById<CardView>(R.id.empty_view)
        excludedAppsRecyclerView.setEmptyView(emptyView)
        excludedAppsRecyclerView.layoutManager = LinearLayoutManager(this)
        val excludedAppsListAdapter = AppListAdapter()
        excludedAppsRecyclerView.adapter = excludedAppsListAdapter

        val notExcludedAppsRecyclerView = findViewById<RecyclerView>(R.id.recycler_not_excluded_apps)
        notExcludedAppsRecyclerView.layoutManager = LinearLayoutManager(this)
        val notExcludedAppsListAdapter = AppListAdapter()
        notExcludedAppsRecyclerView.adapter = notExcludedAppsListAdapter

        val viewModel = ViewModelProviders.of(this).get(AppsViewModel::class.java)
        viewModel.getExcludedApps(appsDao)
            .observe(this,
                Observer<List<App>> { list ->
                    excludedAppsListAdapter.submitList(list)
                }
            )
        viewModel.getNotExcludedApps(appsDao).observe(this,
            Observer<List<App>> { list ->
                notExcludedAppsListAdapter.submitList(list)
            }
        )

        // This enables inertia while scrolling
        excludedAppsRecyclerView.isNestedScrollingEnabled = false
        notExcludedAppsRecyclerView.isNestedScrollingEnabled = false
    }

    override fun onResume() {
        super.onResume()
        appsHandler.updateAppsDatabase()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            super.onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}